#ifndef BUILDERC_H
#define BUILDERC_H

#include "lib.h"
#include "entity.h"
#include "team.h"
#include "map.h"
#include "colorWrapper.h"
#include "entityType.h"
#include "utils.h"
#include "buildingC.h"
#include "teams.h"
#include "aStarAlgo.h"

class BuilderC: public Entity {
    private:
        bool holdingResource;
        int miningTime;
        bool isMining;
        Entity* miningEntity;
        bool isBuilding;
        int buildQueue;
        int buildQueueMax;
        bool isAtBuildLocation;
        
    public:
        BuilderC(int x, int y, int health, int healthMax, EntityType type, ColorWrapper color):Entity(x,y,health,healthMax,type,color) {
            holdingResource = false;
            isMining = false;
            miningTime = 0;
            miningEntity = NULL;
            isBuilding = false;
            isAtBuildLocation = false;
        }
        bool isHoldingResource() {return holdingResource;}
        void setHoldingResource(bool h) { holdingResource = h; }
        void setMiningTime(int time) {miningTime = time;}
        void setIsMining(bool min) { isMining = min;}
        int getMiningTime(){return miningTime;}
        bool getIsMining(){return isMining;}
        Entity* getMiningEntity(){return miningEntity;}
        void setMiningEntity(Entity* e) { miningEntity = e;}
        bool getIsBuilding(){ return isBuilding; }
        void setIsBuilding(bool i) { isBuilding = i; }
        int getBuildQueue() { return buildQueue;}
        void setBuildQueue(int v) { buildQueue = v;}
        int getBuildQueueMax() { return buildQueueMax;}
        void setBuildQueueMax(int v) { buildQueueMax = v;}
        bool getIsAtBuildLocation() { return isAtBuildLocation; }
        void setIsAtBuildLocation(bool i) { isAtBuildLocation = i; }
        void finishedBuilding(Map gameMap, Team* handler);
        void update(Map gameMap, Team* t, Teams handler);
};


#endif
