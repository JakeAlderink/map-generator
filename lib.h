#ifndef LIB_H
#define LIB_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "raylib.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <math.h>
#include <set>
#include <bits/stdc++.h> 

#define HEIGHT 1080
#define WIDTH  1920
#define NUM_OF_RIVERS 4
#define GAME_SCALE 5
#define NUM_OF_INIT_RESOURCES 1000
#define TileHeight HEIGHT/GAME_SCALE
#define TileWidth WIDTH/GAME_SCALE


using namespace std;



#endif