#ifndef TEAM_H
#define TEAM_H

#include "lib.h"
#include "colorWrapper.h"
#include "entity.h"


class Team {
    private:
        int resources;
        ColorWrapper color;
        vector<Entity*>* entities;
    public:
        Team(ColorWrapper c, int r) { color = c; resources = r; entities = new vector<Entity*>();}
        int  getResources(){ return resources; }
        void setResources(int r){ resources = r; }
        ColorWrapper getColor() { return color;}
        void setColor(ColorWrapper newColor) { color = newColor;}
        void add(Entity* entity) { entities->push_back(entity); }
        int entitySize(){ return entities->size(); }
        Entity* getEntity(int index) { return entities->at(index); }
        void remove(int i); 
        void remove(Entity* e);
};

#endif
