#ifndef DRAWUTILS_H
#define DRAWUTILS_H

#include "lib.h"
#include "utils.h"
#include "map.h"
#include "teams.h"
#include "entity.h"
#include "globals.h"

void drawMap(Map gameMap);
void drawEntities(Teams handler);
void drawEntity(Entity* e);
void drawWarrior(int x, int y, int scale, Color color);
void drawResource(int x, int y, int scale);
void drawBuilder(int x, int y, int scale, Color color);
void drawSelectBox(int x, int y, int scale, Color color);
void drawHUDStuff(Teams handler);
void drawResourceNumbers(Teams handler);
void drawHouse(int x, int y, int scale, Color color);
Color determineColor(int terrainType);

#endif