#ifndef MAP_H
#define MAP_H

#include "lib.h"
#include "tile.h"

class Map {
    private:
        map<pair<int,int>, Tile*>* gameMap;
        int width;
        int height;
    public:
        Map(map<pair<int,int>, Tile*>* gameMap, int w, int h){ this->gameMap = gameMap; width = w; height = h;}
        unsigned getWidth(){return width;}
        unsigned getHeight(){return height;}
        bool isInit(int x, int y){return (*gameMap)[pair<int,int>(x,y)]->isInitialized();}
        void setType(int x, int y, int val) { (*gameMap)[pair<int,int>(x,y)]->setType(val);}
        void resetMap();
        int getType(int x,int y){return (*gameMap)[pair<int,int>(x,y)]->getType();}
        Tile* getRandomWaterSource();
        vector<Tile*> getAllWaterTilesNextToEarth();
        vector<Tile*> getAllEarthTilesNextToWater();
        Tile* getTile(int x, int y) { return (*gameMap)[pair<int,int>(x,y)]; }
        vector<Tile*> bottomHalfStartingTiles();
        vector<Tile*> topHalfStartingTiles();
};



#endif