#ifndef MAPGENERATION_H
#define MAPGENERATION_H

#include "lib.h"
#include "map.h"
#include "entry.h"
#include "cardinalDir.h"
#include "teams.h"
#include "team.h"
#include "tile.h"
#include "resourceC.h"
#include "builderC.h"
#include "buildingC.h"

Map generateMap(int width, int height);
void geoform(Map, int, int);
void addToQueue(Entry entry, Map gameMap, vector<Entry*>* queue, int, int);
void createRivers(Map gameMap, int river_numbers);
Tile* goDirection(Map gameMap, Tile* tile, CardinalDir direction);
vector<CardinalDir> getValidDirectionsForARiver(Map gameMap, Tile* tile);
void populateMapWithResources(Map gameMap, Teams handler);
void populateTwoCivs(Map gameMap, Teams handler);
vector<Tile*> findAjacentTiles(vector<Tile*> tiles, Map gameMap);
void simpleCivStartTopHalf(Map gameMap, Team *handler);
void simpleCivStartBottomHalf(Map gameMap, Team *handler);

#endif