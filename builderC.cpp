#include "builderC.h"

void BuilderC::finishedBuilding(Map gameMap, Team* handler) {
    Tile* tile = findValidAdjacent(getX(), getY(), gameMap);
    if(tile == NULL) { return; }
    BuildingC* newBuilding = new BuildingC(tile->getX(), tile->getY(), 100, 100, Building, this->getColor()); 
    handler->add(newBuilding);
    tile->setContainsEntity(true);
    tile->setEntity(newBuilding);
    isBuilding = false;
    isAtBuildLocation = false;
    buildQueue = 0;
    buildQueueMax = 100;
    holdingResource = false;
    isMining = false;
}

void BuilderC::update(Map gameMap, Team* t, Teams handler) {
    
    if(inSaveState) {
        AStarAlgo algo;
        path = algo.continueAStar(saveState, gameMap);
        if(saveState) {
            return;
        }
    }
    
    //curently mining
    if(isMining) {
        if(miningTime > 10) {
            isMining = false;
            holdingResource = true;
            miningTime = 0;
            Entity* e = miningEntity;
            e->setHealth(e->getHealth()-10);
            miningEntity = NULL;
            return;
        }
        miningTime += 1;
    } else if(!holdingResource) {
        if(t->getResources() >= 300 && !isBuilding) {
            //Building new base
            buildQueue = 0;
            buildQueueMax = 100;
            isBuilding = true;
            t->setResources(t->getResources() - 300);
            isAtBuildLocation = false;
            AStarAlgo algo(TileWidth, TileHeight);
            setPath(algo.AStar(this, gameMap, newBuildLocation(gameMap,this)));
            if(inSaveState) {
                return;
            }
        } else if(isBuilding) {
            if(!isAtBuildLocation) {
                followPath(gameMap);
                if(path == NULL || path->getSize() == 0) {
                    isAtBuildLocation = true;
                    path = NULL;
                }
            } else if(buildQueue >= buildQueueMax) {
                finishedBuilding(gameMap, t);
            } else {
                buildQueue += 1;
            }
            return;
        } else {
            //finding next resource
            Entity* e = findClosestEntity(Resource, ColorWrapper(GOLD), this, handler, gameMap);
            if(path == NULL || e != path->getTarget()) {
                AStarAlgo algo(TileWidth, TileHeight);
                path = algo.AStar(this,gameMap,gameMap.getTile(e->getX(),e->getY()));
                if(inSaveState) {
                    return;
                }
            }
            if (absDistance(this,e) <= 1.5) {
                isMining = true;
                miningTime = 0;
                miningEntity = e;
                return;
            }
            followPath(gameMap);
        }
    } else {
        //returning to base
        Entity* e = findClosestEntity(Building, t->getColor(), this, handler, gameMap);
        if(path == NULL || e != path->getTarget()) {
            AStarAlgo algo(TileWidth, TileHeight);
            path = algo.AStar(this,gameMap,gameMap.getTile(e->getX(),e->getY()));
            if(inSaveState) {
                return;
            }
        }
        if(absDistance(this,e) <= 1.5) {
            holdingResource= false;
            miningTime = 0;
            miningEntity = NULL;
            t->setResources(t->getResources()+10);
            return;
        }
        followPath(gameMap);
    }
}