#include "map.h"

 void Map::resetMap() {
    for(int i = 0; i < (int)this->getWidth(); i++) {
        for(int j = 0; j < (int)this->getHeight(); j++) {
            (*gameMap)[pair<int,int>(i,j)]->setInitialized(false);
        }
    }
}


Tile* Map::getRandomWaterSource(){
    vector<Tile*> tiles = getAllWaterTilesNextToEarth();
    Tile* tileChosen = tiles.at(rand() % tiles.size());
    return tileChosen;
}

vector<Tile*> Map::getAllWaterTilesNextToEarth() {
    vector<Tile*> tiles;
    for(int i = 0; i < (int)getWidth(); i++) {
        for(int j = 0; j < (int)getHeight(); j++) {
            if((*gameMap)[pair<int,int>(i,j)]->getType() == 0) { 
                if(((i+1 < (int)getWidth())   && (*gameMap)[pair<int,int>(i+1,j)]->getType() != 0) || 
                   ((i-1 >= 0)           && (*gameMap)[pair<int,int>(i-1,j)]->getType() != 0) || 
                   ((j+1 < (int)getHeight())  && (*gameMap)[pair<int,int>(i,j+1)]->getType() != 0) || 
                   ((j-1 >= 0)           && (*gameMap)[pair<int,int>(i,j-1)]->getType() != 0)) {
                        Tile *tile = (*gameMap)[pair<int,int>(i,j)];
                        tiles.push_back(tile);
                }
            }
        }
    }
    return tiles;
}


vector<Tile*> Map::getAllEarthTilesNextToWater() {
    //Either next to water or completely surround by mointain
    vector<Tile*> tiles;
    for(int i = 0; i < (int)getWidth(); i++) {
        for(int j = 0; j < (int)getHeight(); j++) {
            if((*gameMap)[pair<int,int>(i,j)]->getType() != 0 && !(*gameMap)[pair<int,int>(i,j)]->getContainsEntity()) { 
                if(((i+1 < (int)getWidth())   && (*gameMap)[pair<int,int>(i+1,j)]->getType() == 0) || 
                   ((i-1 >= 0)           && (*gameMap)[pair<int,int>(i-1,j)]->getType() == 0) || 
                   ((j+1 < (int)getHeight())  && (*gameMap)[pair<int,int>(i,j+1)]->getType() == 0) || 
                   ((j-1 >= 0)           && (*gameMap)[pair<int,int>(i,j-1)]->getType() == 0)) {
                        Tile *tile = (*gameMap)[pair<int,int>(i,j)];
                        tiles.push_back(tile);
                } else if(((i+1 < (int)getWidth())   && (*gameMap)[pair<int,int>(i+1,j)]->getType() == 2) && 
                   ((i-1 >= 0)           && (*gameMap)[pair<int,int>(i-1,j)]->getType() == 2) && 
                   ((j+1 < (int)getHeight())  && (*gameMap)[pair<int,int>(i,j+1)]->getType() == 2) && 
                   ((j-1 >= 0)           && (*gameMap)[pair<int,int>(i,j-1)]->getType() == 2)) {
                    Tile *tile = (*gameMap)[pair<int,int>(i,j)];
                    tiles.push_back(tile);
                }
            }
        }
    }
    return tiles;
}


vector<Tile*> Map::topHalfStartingTiles() {
    vector<Tile*> tiles;
    for(int i = 0; i < (int)getWidth(); i++) {
        for(int j = 0; j < (int)getHeight()/2; j++) {
            if((*gameMap)[pair<int,int>(i,j)]->getType() == 1 && !(*gameMap)[pair<int,int>(i,j)]->getContainsEntity()) {
                tiles.push_back((*gameMap)[pair<int,int>(i,j)]);
            }
        }
    }
    return tiles;
}

vector<Tile*> Map::bottomHalfStartingTiles() {
    vector<Tile*> tiles;
    for(int i = 0; i < (int)getWidth(); i++) {
        for(int j = getHeight()/2; j < (int)getHeight(); j++) {
            if((*gameMap)[pair<int,int>(i,j)]->getType() == 1 && !(*gameMap)[pair<int,int>(i,j)]->getContainsEntity()) {
                tiles.push_back((*gameMap)[pair<int,int>(i,j)]);
            }
        }
    }
    return tiles;
}