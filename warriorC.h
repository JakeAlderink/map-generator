#ifndef WARRIORC_H
#define WARRIORC_H

#include "lib.h"
#include "entity.h"
#include "colorWrapper.h"
#include "entityType.h"
#include "teams.h"
#include "team.h"
#include "map.h"
#include "aStarAlgo.h"

class WarriorC: public Entity {
    private:
        int state;
        int damage;
    public:
        WarriorC(int x, int y, int health, int healthMax, EntityType type, ColorWrapper color):Entity(x,y,health,healthMax,type,color) {
            state = 0;
            damage = 1;
        }
        int getState(){ return state; }
        void setState(int s){ state = s; }
        int getDamage() { return damage; }
        void setDamage(int d) { damage = d;}
        void update(Map gameMap, Team* t, Teams handler);
};

#endif