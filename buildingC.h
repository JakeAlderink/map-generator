#ifndef BUILDINGC_H
#define BUILDINGC_H

#include "lib.h"
#include "entity.h"
#include "colorWrapper.h"
#include "tile.h"
#include "map.h"
#include "builderC.h"
#include "entityType.h"
#include "warriorC.h"
#include "teams.h"
#include "team.h"
#include "aStarAlgo.h"

class BuildingC: public Entity {
    private:
        int buildQueue;
        bool isBuilding;
        int buildQueueMax;
        int rotateEntity;
        int damage;
    public:
        BuildingC(int x, int y, int health, int healthMax, EntityType type, ColorWrapper color):Entity(x,y,health,healthMax,type,color) {
            buildQueue = 0;
            isBuilding = false;
            buildQueueMax = 0;
            rotateEntity = 0;
            damage = 10;
        }
        
        friend Tile* findValidAdjacent(int x, int y, Map gameMap);
        int getBuildQueue() { return buildQueue;}
        void setBuildQueue(int v) { buildQueue = v;}
        int getBuildQueueMax() { return buildQueueMax;}
        void setBuildQueueMax(int v) { buildQueueMax = v;}
        bool getIsBuilding(){return isBuilding;}
        void setIsBuilding(bool val) { isBuilding = val;}
        int getDamage(){return damage;}
        void finishedBuilding(Map gameMap, Team* handler);
        void update(Map gameMap, Team* t, Teams handler);
};



#endif
