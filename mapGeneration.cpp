#include "mapGeneration.h"

Map generateMap(int width, int height) {
    map<pair<int,int>,Tile*>* gameMap = new map<pair<int,int>,Tile*>();
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < height; j++) {
            (*gameMap)[pair<int,int>(i,j)] = new Tile(i, j);
        }
    }
    Map newMap(gameMap, width, height);
    return newMap;
}

void geoform(Map gameMap, int width, int height) {
    vector<Entry*>* queue = new vector<Entry*>();
    for(int i = 0; i < 3; i++) {
        queue->push_back(new Entry(rand() % width, rand() % height, 1));
    }
    while(queue->size() != 0) {
        unsigned location = rand() % queue->size();
        Entry *entry = queue->at(location);
        queue->erase(queue->begin() + location);
        if(gameMap.isInit(entry->x, entry->y)) {
            delete entry;
            continue;
        }
        gameMap.setType(entry->x, entry->y, entry->val);
        addToQueue(*entry, gameMap, queue, width, height);
        delete entry;
    }
    delete queue;
    createRivers(gameMap, NUM_OF_RIVERS);
    
}

void createRivers(Map gameMap, int river_numbers) {
    for(int i =0; i < river_numbers; i++) {
        Tile* tile = gameMap.getRandomWaterSource();
        int river_len = (rand()%100) + 10;
        for(int j = 0; j < river_len; j++) {
            vector<CardinalDir> validDirections = getValidDirectionsForARiver(gameMap, tile);
            if(validDirections.size() == 0){
                break;
            }
            CardinalDir direction = validDirections.at(rand() % validDirections.size());
            tile = goDirection(gameMap, tile, direction);
        }
    }
}

void addToQueue(Entry entry, Map gameMap, vector<Entry*>* queue, int width, int height) {
    if ((entry.x + 1) < width && !gameMap.isInit(entry.x+1,entry.y)) {
        int randomValue = rand() % 999;
        if (randomValue == 98) {
            queue->push_back(new Entry(entry.x + 1, entry.y, (entry.val + (rand()%2) + 1 ) % 3));
        } else {
            queue->push_back(new Entry(entry.x + 1, entry.y, entry.val));
        }
    }
    if (entry.x - 1 >= 0 && !gameMap.isInit(entry.x-1,entry.y)) {
        int randomValue = rand() % 999;
        if (randomValue == 98) {
            queue->push_back(new Entry(entry.x - 1, entry.y, (entry.val + (rand()%2) + 1 ) % 3));
        } else {
            queue->push_back(new Entry(entry.x - 1, entry.y, entry.val));
        }
    }    
    if (entry.y + 1 < height && !gameMap.isInit(entry.x,entry.y+1)) {
        int randomValue = rand() % 999;
        if (randomValue == 98) {
            queue->push_back(new Entry(entry.x, entry.y + 1, (entry.val + (rand()%2) + 1 ) % 3));
        } else {
            queue->push_back(new Entry(entry.x, entry.y + 1, entry.val));
        }
    }
    if (entry.y - 1 >= 0 && !gameMap.isInit(entry.x,entry.y-1)) {
        int randomValue = rand() % 999;
        if (randomValue == 98) {
            queue->push_back(new Entry(entry.x, entry.y - 1, (entry.val + (rand()%2) + 1 ) % 3));
        } else {
            queue->push_back(new Entry(entry.x, entry.y - 1, entry.val));
        }
    }
}

vector<CardinalDir> getValidDirectionsForARiver(Map gameMap, Tile* tile) {
    vector<CardinalDir> vec;
    if(tile->getY() - 1 >= 0 && gameMap.getType(tile->getX(),tile->getY() - 1) != 0) {
        vec.push_back(NORTH);
    }
    if(tile->getY() + 1 < (int)gameMap.getHeight() && gameMap.getType(tile->getX(),tile->getY() + 1) != 0) {
        vec.push_back(SOUTH);
    }
    if(tile->getX() - 1 >= 0 && gameMap.getType(tile->getX() - 1,tile->getY()) != 0) {
        vec.push_back(WEST);
    }
    if(tile->getX() + 1 < (int)gameMap.getWidth() && gameMap.getType(tile->getX() + 1, tile->getY()) != 0) {
        vec.push_back(EAST);
    }
    return vec;
}


Tile* goDirection(Map gameMap, Tile* tile, CardinalDir direction) {
    Tile* returnTile = NULL;
    if(direction == NORTH) {
        gameMap.setType(tile->getX(), tile->getY() - 1, 0);
        returnTile = gameMap.getTile(tile->getX(), tile->getY()-1);
    } else if(direction == SOUTH) {
        gameMap.setType(tile->getX(), tile->getY() + 1, 0);
        returnTile = gameMap.getTile(tile->getX(), tile->getY()+1);
    } else if(direction == WEST) {
        gameMap.setType(tile->getX() - 1, tile->getY(), 0);
        returnTile = gameMap.getTile(tile->getX() - 1, tile->getY());
    } else if(direction == EAST) {
        gameMap.setType(tile->getX() + 1, tile->getY(), 0);
        returnTile = gameMap.getTile(tile->getX() + 1, tile->getY());
    }
    return returnTile;
}

void populateMapWithResources(Map gameMap, Teams handler) {
    vector<Tile*> tiles = gameMap.getAllEarthTilesNextToWater();
    for(int i = 0; i < NUM_OF_INIT_RESOURCES && tiles.size() > 0; i++) {
        int index = rand() % tiles.size();
        Tile* randomTile = tiles.at(index);
        tiles.erase(tiles.begin()+ index);
        Entity* e = new ResourceC(randomTile->getX(), randomTile->getY(), 100, 100, Resource, ColorWrapper(GOLD));
        handler.getTeam(ColorWrapper(GOLD))->add(e);
        
        randomTile->setContainsEntity(true);
        randomTile->setEntity(e);
    }
}

void populateTwoCivs(Map gameMap, Teams handler) {
    simpleCivStartBottomHalf(gameMap, handler.getTeam(ColorWrapper(BLUE)));
    simpleCivStartTopHalf(gameMap, handler.getTeam(ColorWrapper(RED)));
}

void simpleCivStartBottomHalf(Map gameMap, Team* team) {
    vector<Tile*> tiles = gameMap.bottomHalfStartingTiles();
    vector<Tile*> adjacentTiles = findAjacentTiles(tiles, gameMap);
    Entity* e1 = new BuilderC(adjacentTiles[0]->getX(), adjacentTiles[0]->getY(), 100, 100, Builder, team->getColor());
    Entity* e2 = new BuildingC(adjacentTiles[1]->getX(), adjacentTiles[1]->getY(), 100, 100, Building, team->getColor());
    team->add(e1);
    team->add(e2);
    adjacentTiles[0]->setContainsEntity(true);
    adjacentTiles[1]->setContainsEntity(true);
    adjacentTiles[0]->setEntity(e1);
    adjacentTiles[1]->setEntity(e2);
}

void simpleCivStartTopHalf(Map gameMap, Team* team) {
    vector<Tile*> tiles = gameMap.topHalfStartingTiles();
    vector<Tile*> adjacentTiles = findAjacentTiles(tiles, gameMap);
    Entity* e1 = new BuilderC(adjacentTiles[0]->getX(), adjacentTiles[0]->getY(), 100, 100, Builder, team->getColor());
    Entity* e2 = new BuildingC(adjacentTiles[1]->getX(), adjacentTiles[1]->getY(), 100, 100, Building, team->getColor());
    team->add(e1);
    team->add(e2);
    adjacentTiles[0]->setContainsEntity(true);
    adjacentTiles[1]->setContainsEntity(true);
    adjacentTiles[0]->setEntity(e1);
    adjacentTiles[1]->setEntity(e2);
}


vector<Tile*> findAjacentTiles(vector<Tile*> tiles, Map gameMap) {
    vector<Tile*> adjacentTiles;
    while(adjacentTiles.size() < 2) {
        Tile* randomTile = tiles.at(rand()%tiles.size());
        int i = randomTile->getX();
        int j = randomTile->getY();
        if(((i+1 < (int)gameMap.getWidth())  && gameMap.getTile(i+1,j)->getType() == 1 && !gameMap.getTile(i+1,j)->getContainsEntity())) {
            adjacentTiles.push_back(randomTile);
            adjacentTiles.push_back(gameMap.getTile(i+1,j));
            return adjacentTiles;
        }
        if(((i-1 >= 0)              && gameMap.getTile(i-1,j)->getType() == 1 && !gameMap.getTile(i-1,j)->getContainsEntity())) { 
            adjacentTiles.push_back(randomTile);
            adjacentTiles.push_back(gameMap.getTile(i-1,j));
            return adjacentTiles;
        } 
        if(((j+1 < (int)gameMap.getHeight()) && gameMap.getTile(i,j+1)->getType() == 1 && !gameMap.getTile(i,j+1)->getContainsEntity())) {
            adjacentTiles.push_back(randomTile);
            adjacentTiles.push_back(gameMap.getTile(i,j+1));
            return adjacentTiles;
        }           
        if(((j-1 >= 0)              && gameMap.getTile(i,j-1)->getType() == 1 && !gameMap.getTile(i,j-1)->getContainsEntity())) {
            adjacentTiles.push_back(randomTile);
            adjacentTiles.push_back(gameMap.getTile(i,j-1));
            return adjacentTiles;
        }
    }
    return adjacentTiles;
}