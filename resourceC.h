#ifndef RESOURCEC_H
#define RESOURCEC_H

#include "lib.h"
#include "entity.h"
#include "colorWrapper.h"
#include "entityType.h"
#include "teams.h"
#include "team.h"
#include "map.h"


class ResourceC: public Entity {
    public:
        ResourceC(int x, int y, int health, int healthMax, EntityType type, ColorWrapper color):Entity(x,y,health,healthMax,type,color) {
            
        }
        void update(Map gameMap, Team* t, Teams handler) {
            
        }
};

#endif
