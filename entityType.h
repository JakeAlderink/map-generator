#ifndef ENTITYTYPE_H
#define ENTITYTYPE_H


enum EntityType {
    Building = 1,
    Builder = 2,
    Warrior = 3,
    Resource = 4
};

#endif