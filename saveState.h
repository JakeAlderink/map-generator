#ifndef SAVESTATE_H
#define SAVESTATE_H

#include "aStarAlgo.h"
#include "map.h"
#include "tile.h"
#include "entity.h"

class AStarAlgo;
class Tile;
class Entity;
class Map;


class SaveState {
public:
    map<pair<int,int>, float> pathDistance;
    map<pair<int,int>, float> absoluteDistance;
    map<pair<int,int>, float> f;
    float maxValue;
    int width;
    int height;
    Entity* start;
    Tile* target;
    set<pair<float, pair<int,int>>> sortedStack; 
    SaveState(AStarAlgo algo, Entity* start, Tile* target, set<pair<float ,pair<int,int>>> sortedStack);
    
};
    
#endif