#ifndef TILE_H
#define TILE_H

#include "lib.h"
#include "entity.h"

class Entity;

class Tile {
    private:
        bool initialized;
        int type;
        int x;
        int y;
        bool containsEntity;
        Entity* containedEntity;
    public:
        Tile(int x_v, int y_v){this->x = x_v; this->y = y_v; initialized = false; type = -1; containsEntity = false; containedEntity = NULL;}
        Entity* getEntity(){ return containedEntity; }
        void setEntity(Entity* e) { containedEntity = e;}
        int getType(){return type;}
        bool isInitialized(){return initialized;}
        void setType(int type){this->type = type; this->initialized = true;}
        void setInitialized(bool val){initialized = val;}
        int getX(){ return x; }
        int getY(){ return y; }
        void setX(int x){ this->x = x;}
        bool getContainsEntity() {return containsEntity;}
        void setContainsEntity(bool val) { containsEntity = val; containedEntity = NULL;}
};


#endif