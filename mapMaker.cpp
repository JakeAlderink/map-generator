#include "mapMaker.h"

int showMap(Map gameMapCode, Teams handler);
void updateAllEntities(Map gameMap, Teams handler);
void processInput(Map gameMap, Teams handler);

set<Entity*> currSelected;

int main(void) {
    srand(time(0));
    Map gameMap = generateMap(TileWidth, TileHeight);
    geoform(gameMap, TileWidth, TileHeight);
    Teams handler;
    handler.add(new Team(ColorWrapper(RED),100));
    handler.add(new Team(ColorWrapper(BLUE),100));
    handler.add(new Team(ColorWrapper(GOLD),100));
    
    populateMapWithResources(gameMap, handler);
    populateTwoCivs(gameMap, handler);
    return showMap(gameMap, handler);
}

int showMap(Map gameMap, Teams handler) {
    const int screenWidth = WIDTH;
    const int screenHeight = HEIGHT;

    InitWindow(screenWidth, screenHeight, "civ sim");
    SetTargetFPS(60);
    unsigned long i = 0;
    while (!WindowShouldClose())  
    {
        i++;
        processInput(gameMap, handler);

        BeginDrawing();
            ClearBackground(RAYWHITE);
            drawMap(gameMap);
            drawEntities(handler);
            drawHUDStuff(handler);
        EndDrawing();
        updateAllEntities(gameMap, handler);
    }
    

    CloseWindow();

    return 0;
}

void updateAllEntities(Map gameMap, Teams handler) {
    for(auto t =handler.getTeams()->begin(); t != handler.getTeams()->end(); t++) {
        int warrior_count = 0;
        for(int i = 0; i < (*t)->entitySize(); i++) {
            (*t)->getEntity(i)->update(gameMap, *t, handler);
            if((*t)->getEntity(i)->getType() == Warrior) {
                warrior_count++;
            }
        }
        if(warrior_count > 8) {
            for(int i = 0; i < (*t)->entitySize(); i++) {
                if((*t)->getEntity(i)->getType() == Warrior) {
                    dynamic_cast<WarriorC*>((*t)->getEntity(i))->setState(1);
                }   
            }
        } else {
            for(int i = 0; i < (*t)->entitySize(); i++) {
                if((*t)->getEntity(i)->getType() == Warrior) {
                    dynamic_cast<WarriorC*>((*t)->getEntity(i))->setState(0);
                }   
            }
        }
    }
    for(auto t =handler.getTeams()->begin(); t != handler.getTeams()->end(); t++) {
        for(int i = 0; i < (*t)->entitySize(); i++) {        
            if((*t)->getEntity(i)->getHealth() < 0) {
                gameMap.getTile((*t)->getEntity(i)->getX(), (*t)->getEntity(i)->getY())->setContainsEntity(false);
                (*t)->remove(i);
                i--;
            }
        }
    }
}

void processInput(Map gameMap, Teams handler) {
    static bool leftMousePressed = false;
    static Vector2* startingPos = NULL;
    if(IsKeyPressed(KEY_DELETE)) {
        while(currSelected.size() != 0) {
            Entity* e = *currSelected.begin();
            currSelected.erase(currSelected.begin());
            Team* t = handler.getTeam(e->getColor());
            gameMap.getTile(e->getX(), e->getY())->setContainsEntity(false);
            t->remove(e);
        }
    }
    if(IsKeyPressed(KEY_F)) {
        ToggleFullscreen();
    }
    if(IsMouseButtonDown(MOUSE_LEFT_BUTTON) && !leftMousePressed){
        leftMousePressed = true;
        currSelected.clear();
        Vector2* tmp = new Vector2;
        Vector2 mPos = GetMousePosition();
        tmp->x = mPos.x;
        tmp->y = mPos.y;
        if(startingPos != NULL) {
            delete startingPos;
        }
        startingPos = tmp;
    } else if(!IsMouseButtonDown(MOUSE_LEFT_BUTTON) && leftMousePressed && startingPos != NULL) {
        leftMousePressed = false;
        Vector2 mPos = GetMousePosition();
        int leftMostX = ((int)mPos.x)/GAME_SCALE;
        int topMostY = ((int)mPos.y)/GAME_SCALE;
        int rightMostX = ((int)mPos.x)/GAME_SCALE;
        int bottomMostY= ((int)mPos.y)/GAME_SCALE;

        if( ((int)startingPos->x)/GAME_SCALE < leftMostX) {
            leftMostX = ((int)startingPos->x)/GAME_SCALE;
        } else {
            rightMostX = ((int)startingPos->x)/GAME_SCALE;
        }
        if( ((int)startingPos->y)/GAME_SCALE < topMostY) {
            topMostY = ((int)startingPos->y)/GAME_SCALE;
        } else {
            bottomMostY = ((int)startingPos->y)/GAME_SCALE;
        }
        
        for(int i = leftMostX; i <= rightMostX; i++) {
            for(int j = topMostY; j <= bottomMostY; j++) {
                Tile* selectedTile = gameMap.getTile(i, j);
                if(selectedTile->getContainsEntity()) {
                    currSelected.insert(selectedTile->getEntity());
                }
            }
        }
    } else if(!IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
        leftMousePressed = false;
    }
}