#include "utils.h"

bool isValid(int x, int y, Map gameMap) {
    return (x >= 0 && x < (int)gameMap.getWidth() && y >=0 && y < (int)gameMap.getHeight() && !gameMap.getTile(x,y)->getContainsEntity() && gameMap.getType(x,y) != 0);
}

double absDistance(Entity* e, Entity* k) {
    return sqrt(pow((double)e->getX()-(double)k->getX(),2) + pow((double)e->getY()-(double)k->getY(),2));
}

float absDistance(Tile* e, Entity* k) {
	return sqrt(pow((float)e->getX()-(float)k->getX(),2) + pow((float)e->getY()-(float)k->getY(),2));
}

float absDistance(Tile* e, Tile* k) {
	return sqrt(pow((float)e->getX()-(float)k->getX(),2) + pow((float)e->getY()-(float)k->getY(),2));
}

double absSingleDistance(float x, float y) {
    return fabs(y-x);
}


Tile* findValidAdjacent(int x, int y, Map gameMap) {
    if(((x+1 < (int) gameMap.getWidth())  && gameMap.getTile(x+1,y)->getType() == 1 && !gameMap.getTile(x+1,y)->getContainsEntity())) {
            return gameMap.getTile(x+1, y);
        }
        if(((x-1 >= 0)              && gameMap.getTile(x-1,y)->getType() == 1 && !gameMap.getTile(x-1,y)->getContainsEntity())) { 
            return gameMap.getTile(x-1, y);
        } 
        if(((y+1 < (int) gameMap.getHeight()) && gameMap.getTile(x,y+1)->getType() == 1 && !gameMap.getTile(x,y+1)->getContainsEntity())) {
            return gameMap.getTile(x, y+1);
        }           
        if(((y-1 >= 0)              && gameMap.getTile(x,y-1)->getType() == 1 && !gameMap.getTile(x,y-1)->getContainsEntity())) {
            return gameMap.getTile(x, y-1);
        }
    return NULL;
}

Vector2 initVector2(float x, float y) {
    Vector2 retVec;
    retVec.x = (float) x;
    retVec.y = (float) y;
    return retVec;
}

Tile* newBuildLocation(Map gameMap, Entity* buildingBoi) {
    set<Tile*> tiles25PathDistancesAway;
    for(int i = -25; i <26; i++) {
        int j1 = (abs(i) - 25);
        int j2 = -(abs(i) - 25);
        if(isValid(i,j1, gameMap)) {
            tiles25PathDistancesAway.insert(gameMap.getTile(buildingBoi->getX() + i,buildingBoi->getY() + j1));
        }
        if(isValid(i,j2, gameMap)) {
            tiles25PathDistancesAway.insert(gameMap.getTile(buildingBoi->getX() + i,buildingBoi->getY() + j2));
        }
    }
    int randIndex = rand() % tiles25PathDistancesAway.size();
    auto it = tiles25PathDistancesAway.begin();
    advance(it,randIndex);
    return *it;   
}



Entity* findClosestEnemy(ColorWrapper enemyColor, Entity* currEntity, Teams handler, Map gameMap) {
    Entity* WaE = findClosestEntity(Warrior, enemyColor, currEntity, handler, gameMap);
    Entity* BuE = findClosestEntity(Building, enemyColor, currEntity, handler, gameMap);
    Entity* BiE = findClosestEntity(Builder, enemyColor, currEntity, handler, gameMap);
    if(WaE != NULL && BiE != NULL && absDistance(currEntity, WaE) < absDistance(currEntity, BuE) && absDistance(currEntity, WaE) < absDistance(currEntity, BiE)) {
        return WaE;
    }
    if(BiE != NULL && absDistance(currEntity, BiE) < absDistance(currEntity, BuE)) {
       return BiE;
    }
    return BuE;
}

Entity* findClosestEntity(EntityType et, ColorWrapper c, Entity* currEntity, Teams handler, Map gameMap) {
    Team* t = handler.getTeam(c);
    double currClosest = WIDTH*HEIGHT;
    Entity* e = NULL;
    for(int i = 0; i < t->entitySize(); i++) {
        Entity* toCheck = t->getEntity(i);
        if(toCheck->getType() != et || 
            (!isValid(toCheck->getX()+ 1,toCheck->getY(), gameMap) && !isValid(toCheck->getX()- 1,toCheck->getY(), gameMap) && !isValid(toCheck->getX(),toCheck->getY()+1, gameMap) && !isValid(toCheck->getX(),toCheck->getY()-1, gameMap))) {
            continue;
        }
        double x = absDistance(currEntity, toCheck);
        if(currClosest >= x) {
            currClosest = x;
            e = toCheck;
        }
    }
    return e;
}