#include "drawUtils.h"

void drawMap(Map gameMap) {
    for(int i = 0; i < (int)gameMap.getWidth(); i++) {
        for(int j = 0; j < (int)gameMap.getHeight(); j++) {
            DrawRectangle(i*GAME_SCALE, j*GAME_SCALE, GAME_SCALE, GAME_SCALE, determineColor(gameMap.getType(i,j)));
        }   
    }    
}

void drawEntities(Teams handler) {
    for(auto t =handler.getTeams()->begin(); t != handler.getTeams()->end(); t++) {
        for(int i = 0; i < (*t)->entitySize(); i++) {
            drawEntity((*t)->getEntity(i));
        }
    }
}

void drawEntity(Entity* e) {
    if(e->getType() == Building) {
        drawHouse(e->getX()*GAME_SCALE, e->getY()*GAME_SCALE, GAME_SCALE, e->getColor().getColor());
    } else if(e->getType() == Builder) {
        drawBuilder(e->getX()*GAME_SCALE, e->getY()*GAME_SCALE, GAME_SCALE, e->getColor().getColor());
    } else if(e->getType() == Warrior) {
        drawWarrior(e->getX()*GAME_SCALE, e->getY()*GAME_SCALE, GAME_SCALE, e->getColor().getColor());
    } else if(e->getType() == Resource) {
        drawResource(e->getX()*GAME_SCALE, e->getY()*GAME_SCALE, GAME_SCALE);
    }
    if(currSelected.find(e) != currSelected.end()) {
        drawSelectBox(e->getX()*GAME_SCALE, e->getY()*GAME_SCALE, GAME_SCALE, e->getColor().getColor());
    }
}



void drawHUDStuff(Teams handler) {
    drawResourceNumbers(handler);
}

void drawResourceNumbers(Teams handler) {
    vector<Team*>* teams = handler.getTeams();
    int xPos = 50;
    int yPos = 50;
    int rFontSize = 30;
    for(auto t = teams->begin(); t != teams->end(); t++ ) {
        DrawText(to_string((*t)->getResources()).c_str(),xPos, yPos, rFontSize, (*t)->getColor().getColor());
        yPos += 40;
    }
}

void drawSelectBox(int x, int y, int scale, Color color) {
    DrawRectangleLines(x,y,scale,scale,color);
}

void drawWarrior(int x, int y, int scale, Color color) {
    //head
    DrawRectangle( x, y, (2.0/5.0)*scale, (2.0/5.0)*scale, GRAY);
    //body
    DrawRectangle( x , y+ (2.0/5.0)*scale, (2.0/5.0)*scale, (3.0/5.0)*scale, color);
    //weapon?
    DrawLineEx(initVector2(x + (3.0/5.0)*scale, y + (3.0/5.0)*scale), initVector2( x , y + (5.0/5.0)*scale), scale* (1.0/5.0), LIGHTGRAY);
}


void drawHouse(int x, int y, int scale, Color color) {
    DrawRectangle( (x+(1.0/5.0*scale)),  y+(2.0/5.0*scale), (3.0/5.0*scale), (2.0/5.0*scale), color);
    
    //Counter-clockwise cause thats what raylib needs.
    DrawTriangle(initVector2((x+(5.0/5.0*scale)), (y+(2.0/5.0)*scale)), initVector2((x+(3.0/5.0)*scale), y), initVector2(x, (y+(2.0/5.0)*scale)), BROWN);
}

void drawResource(int x, int y, int scale) {
    DrawTriangle(initVector2((x+(4.0/5.0*scale)), (y+(5.0/5.0)*scale)), initVector2((x+(2.0/5.0)*scale), (y+(3.0/5.0)*scale)), initVector2(x, (y+(5.0/5.0)*scale)), GOLD);
}
 
void drawBuilder(int x, int y, int scale, Color color) {
    //head
    DrawRectangle( x, y, (2.0/5.0)*scale, (2.0/5.0)*scale, GRAY);
    //body
    DrawRectangle( x , y+ (2.0/5.0)*scale, (2.0/5.0)*scale, (2.0/5.0)*scale, color);
    //tool
    DrawRectangle( x + (4.0/5.0)*scale, y, (2.0/5.0)*scale, (2.0/5.0)*scale, SKYBLUE);
    DrawLineEx(initVector2(x + (3.0/5.0)*scale, y + (3.0/5.0)*scale), initVector2( x , y + (5.0/5.0)*scale), scale* (1.0/5.0), BROWN);
}


Color determineColor(int terrainType) {
    switch(terrainType){
        case 0:
            return BLUE;
            break;
        case 1:
            return GREEN;
            break;
        case 2:
            return DARKGRAY;
            break;
        default:
            return RAYWHITE;
    }
}
