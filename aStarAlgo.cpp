#include "aStarAlgo.h"

AStarAlgo::AStarAlgo(int mapWidth, int mapHeight) {
    width = mapWidth;
    height = mapHeight;
    maxValue = (float)mapWidth*mapHeight;
    for(int i = 0; i < mapWidth; i++) {
        for(int j = 0; j < mapHeight; j++) {
            pathDistance[pair<int,int>(i,j)] = maxValue;
            absoluteDistance[pair<int,int>(i,j)] = maxValue;
            f[pair<int,int>(i,j)] = maxValue;
        }
    }
}

Path* AStarAlgo::continueAStar(SaveState* saveState, Map gameMap) {
    pathDistance = saveState->pathDistance;
    absoluteDistance = saveState->absoluteDistance;
    f = saveState->f;
    maxValue = saveState->maxValue;
    width = saveState->width;
    height = saveState->height;
    std::set<std::pair<float ,std::pair<int,int>>> sortedStack = saveState->sortedStack;
    Entity* currEntity  = saveState->start;
    Tile* target = saveState->target;
    delete saveState;
    currEntity->setSaveState(NULL);
    currEntity->setInSaveState(false);
    return mainProcessLoop(sortedStack, currEntity, target, gameMap);
}

Path* AStarAlgo::mainProcessLoop(std::set<std::pair<float ,std::pair<int,int>>> sortedStack, Entity* currEntity, Tile* target, Map gameMap) {
    int i = 0;
    while(sortedStack.size() > 0 && f[pair<int,int>(target->getX(),target->getY())] == maxValue) {
        i++;
        auto entry = *sortedStack.begin();
        sortedStack.erase(entry);
		int x = entry.second.first;
		int y = entry.second.second;
		float fVal = entry.first;
        float pathDist = pathDistance[pair<int,int>(x,y)];
        float currAbsDist = absoluteDistance[pair<int,int>(x,y)];
        //cout << "pathDistance " << pathDist << "\n" << endl;
        if(i == 51) {
            //currEntity->setSaveState(new SaveState(*this, currEntity, target, sortedStack));
            //currEntity->setInSaveState(true);
            return new Path(target->getEntity(), this->producePath(gameMap.getTile(x,y)));
        }
        if(currAbsDist == 1.0) {
            pathDistance[pair<int,int>(target->getX(),target->getY())] = pathDist + 1.0;
            break;
        }
		
		
		
		pair<int,int> eastCoord = pair<int,int>(x+1,y);
		pair<int,int> westCoord = pair<int,int>(x-1,y);
		pair<int,int> southCoord = pair<int,int>(x,y+1);
		pair<int,int> northCoord = pair<int,int>(x,y-1);
		
		
        if(isValid(x+1, y, gameMap) && f[eastCoord] == maxValue) {
			pathDistance[eastCoord] = pathDist + 1.0;
			absoluteDistance[eastCoord] = absDistance(gameMap.getTile(x+1,y), target);
			f[eastCoord] = pathDistance[eastCoord] + absoluteDistance[eastCoord];
			sortedStack.insert(pair<float,pair<int,int>>(absoluteDistance[eastCoord], pair<int,int>(x+1, y)));
        }
        if(isValid(x-1, y, gameMap) && f[westCoord] == maxValue) {
			pathDistance[westCoord] = pathDist + 1.0;
			absoluteDistance[westCoord] = absDistance(gameMap.getTile(x-1,y), target);
			f[westCoord] = pathDistance[westCoord] + absoluteDistance[westCoord];
			sortedStack.insert(pair<float,pair<int,int>>(absoluteDistance[westCoord], pair<int,int>(x-1, y)));
        }
        if(isValid(x, y+1, gameMap) && f[southCoord] == maxValue) {
			pathDistance[southCoord] = pathDist + 1.0;
			absoluteDistance[southCoord] = absDistance(gameMap.getTile(x,y+1), target);
			f[southCoord] = pathDistance[southCoord] + absoluteDistance[southCoord];
			sortedStack.insert(pair<float,pair<int,int>>(absoluteDistance[southCoord], pair<int,int>(x, y+1)));
        }
        if(isValid(x, y-1, gameMap) && f[northCoord] == maxValue) {
			pathDistance[northCoord] = pathDist + 1.0;
			absoluteDistance[northCoord] = absDistance(gameMap.getTile(x,y-1), target);
			f[northCoord] = pathDistance[northCoord] + absoluteDistance[northCoord];
			sortedStack.insert(pair<float,pair<int,int>>(absoluteDistance[northCoord], pair<int,int>(x, y-1)));
        }
    }
    return new Path(target->getEntity(), this->producePath(target));
}


Path* AStarAlgo::AStar(Entity* currEntity, Map gameMap, Tile* target) {
    std::set<std::pair<float ,std::pair<int,int>>> sortedStack;
    sortedStack.insert(pair<float,pair<int,int>>(0.0, pair<int,int>(currEntity->getX(), currEntity->getY())));
	pathDistance[pair<int,int>(currEntity->getX(), currEntity->getY())] = 0.0;
	absoluteDistance[pair<int,int>(currEntity->getX(), currEntity->getY())] = 0.0;
	f[pair<int,int>(currEntity->getX(), currEntity->getY())] = 0.0;
    return mainProcessLoop(sortedStack, currEntity, target, gameMap);    
}

vector<pair<int,int> > AStarAlgo::producePath(Tile* target) {
    vector<pair<int,int> > path;
    int currX = target->getX();
    int currY = target->getY();
    float currPathIndex = pathDistance[pair<int,int>(currX, currY)];
    path.push_back(pair<int,int>(currX,currY));
    while(currPathIndex != 0.0) {
		pair<int,int> eastCoord = pair<int,int>(currX+1, currY);
		pair<int,int> westCoord = pair<int,int>(currX-1, currY);
		pair<int,int> southCoord = pair<int,int>(currX, currY+1);
		pair<int,int> northCoord = pair<int,int>(currX, currY-1);
        if(currX+1 >= 0 && currX+1 < width && currY >=0 && currY < height && pathDistance[eastCoord] == currPathIndex-1.0) {
            currX++;
            currPathIndex-=1.0;
            path.push_back(pair<int,int>(currX,currY));
        }
        else if(currX-1 >= 0 && currX-1 < width && currY >=0 && currY < height && pathDistance[westCoord] == currPathIndex-1.0) {
            currX--;
            currPathIndex-=1.0;
            path.push_back(pair<int,int>(currX,currY));
        }
        else if(currX >= 0 && currX < width && currY+1 >=0 && currY+1 < height && pathDistance[southCoord] == currPathIndex-1.0) {
            currY++;
            currPathIndex-=1.0;
            path.push_back(pair<int,int>(currX,currY));
        }
        else if(currX >= 0 && currX < width && currY-1 >=0 && currY-1 < height && pathDistance[northCoord] == currPathIndex-1.0) {
            currY--;
            currPathIndex-=1.0;
            path.push_back(pair<int,int>(currX,currY));
        } else {
            path.clear();
            return path;
        }
    }
    return path;
}