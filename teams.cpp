#include "teams.h"


void Teams::add(Team* team) { 
    teams->push_back(team); 
    colors->push_back(team->getColor()); 
}

Team* Teams::getTeam(ColorWrapper color) {
    int i = 0;
    for(i = 0; i < (int)colors->size(); i++) {
        if(colors->at(i) == color) {
            break;
        }
    }
    return teams->at(i); 
}

Teams::Teams(){
    teams = new vector<Team*>();
    colors = new vector<ColorWrapper>();
}