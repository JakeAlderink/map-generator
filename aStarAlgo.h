#ifndef ASTARALGO_H
#define ASTARALGO_H

#include "lib.h"
#include "path.h"
#include "entity.h"
#include "tile.h"
#include "map.h"
#include "utils.h"
#include "saveState.h"

class SaveState;
class Tile;
class Map;
class Path;

class AStarAlgo {
	private:
		map<pair<int,int>, float> pathDistance;
		map<pair<int,int>, float> absoluteDistance;
		map<pair<int,int>, float> f;
		float maxValue;
		int width;
		int height;
		vector<pair<int,int>> producePath(Tile*);
        Path* mainProcessLoop(std::set<std::pair<float ,std::pair<int,int>>> sortedStack, Entity* currEntity, Tile* target, Map gameMap);
	public:
        AStarAlgo() {}
        /* only use if coming from saveState */
		AStarAlgo(int mapWidth, int mapHeight);
        Path* continueAStar(SaveState* s, Map gameMap);
		Path* AStar(Entity* currEntity, Map gameMap, Tile* target);
        map<pair<int,int>, float> getPathDistance() { return pathDistance; }
        map<pair<int,int>, float> getAbsoluteDistance() { return absoluteDistance; }
        map<pair<int,int>, float> getF() { return f; }
        float getMaxValue() { return maxValue; }
        int getWidth() { return width; }
        int getHeight() { return height; }
        
};


#endif