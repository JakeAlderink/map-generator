#include "warriorC.h"


void WarriorC::update(Map gameMap, Team* t, Teams handler) {
    /*Warrior states:
    Patrol(0):
        at least 5 tiles away from closest friendly building.
        at most 20 tiles away from closests friendly building.
        if an enemy entity is within 15 tiles away from warrior then attack.
    Attack(1):
        10-15 warriors on the team means attack the closest entity.
    */
    if(inSaveState) {
        AStarAlgo algo;
        path = algo.continueAStar(saveState, gameMap);
        if(inSaveState) {
            return;
        }
    }
    ColorWrapper enemyColor(RED);
    if(t->getColor() == enemyColor) {
        enemyColor.setColor(BLUE);
    }
    
    if(state == 0) {
        Entity* closestE = findClosestEnemy(enemyColor, this, handler, gameMap);
        float distanceToE = absDistance(this, closestE);
        if(distanceToE <= 15.0) {
            if(distanceToE <= 1.5) {
                closestE->setHealth(closestE->getHealth() - damage);
                return;
            }
            goTo(closestE, gameMap);
            return;
        }
        Entity* closestF = findClosestEntity(Building, color, this, handler, gameMap);
        float distanceToF = absDistance(closestF, this);
        if(distanceToF <= 5.0) {
            goAwayFrom(closestF, gameMap);
            return;
        }
        if(distanceToF >= 20.0) {
            goTo(closestF, gameMap);
            return;
        }
        goRandomDirection(gameMap);
    } else if (state == 1) {
        Entity* closestE = findClosestEnemy(enemyColor, this, handler, gameMap);
        float distanceToE = absDistance(this, closestE);
        if(path == NULL || closestE != path->getTarget()) {
            AStarAlgo algo(TileWidth, TileHeight);
            path = algo.AStar(this, gameMap, gameMap.getTile(closestE->getX(),closestE->getY()));
            if(inSaveState) { 
                return;
            }
        }
        if(distanceToE <= 1.5) {
            closestE->setHealth(closestE->getHealth() - damage);
            return;
        }
        followPath(gameMap);
        return;
    }
}