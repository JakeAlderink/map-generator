#ifndef ENTITY_H
#define ENTITY_H

#include "lib.h"
#include "map.h"
#include "entityType.h"
#include "colorWrapper.h"
#include "path.h"
#include "cardinalDir.h"
#include "utils.h"
#include "teams.h"
#include "team.h"
#include "saveState.h"

class SaveState;
class Team;
class Teams;
class Path;
class Map;

class Entity {
    protected:
        int x;
        int y;
        int health;
        int healthMax;
        EntityType type;
        ColorWrapper color;
        Path* path;
        bool inSaveState;
        SaveState* saveState;
        
    public:
        void goTo(Entity* e, Map gameMap);
        void goAwayFrom(Entity* e, Map gameMap);
        void goRandomDirection(Map gameMap);
        virtual ~Entity();
        Entity(int x, int y, int health, int healthMax, EntityType type, ColorWrapper color);
        void setX(int x) {this->x = x;}
        void setY(int y) {this->y = y;}
        void setHealth(int health) {this->health = health ;}
        void setHealthMax(int healthMax) {this->healthMax = healthMax ;}
        void setType(EntityType type) {this-> type = type;}
        void setColor(ColorWrapper color) {this->color = color;}
        Path* getPath(){return path;}
        void setPath(Path* p);
        int getX(){ return x;}
        int getY(){ return y;}
        int getHealth(){ return health;}
        int getHealthMax(){ return healthMax;}
        EntityType getType(){ return type;}
        ColorWrapper getColor(){ return color;}
        void followPath(Map gameMap);
        virtual void update(Map gameMap, Team* t, Teams handler);
        void setSaveState(SaveState* s){ saveState = s;}
        SaveState* getSaveState() { return saveState; }
        void setInSaveState(bool s) { inSaveState = s; }
        bool getInSaveState() { return inSaveState; }
};




#endif
