#include "entity.h"


Entity::Entity(int x, int y, int health, int healthMax, EntityType type, ColorWrapper color){
    this->x =x; 
    this->y = y; 
    this->health =health; 
    this->healthMax =healthMax; 
    this->type =type; 
    this -> color = color; 
    path = NULL;
    saveState = NULL;
    inSaveState = false;
}

Entity::~Entity() { if(path != NULL) { delete path;}}

void Entity::update(Map gameMap, Team* t, Teams handler) {
    cout << "shouldn't have been called..." << endl;
}

void Entity::setPath(Path* p){ 
    if(path != NULL) { 
        delete path; 
        path = NULL; 
    } 
    path = p;
}
        

void Entity::followPath(Map gameMap) {
    if(path->getSize() == 0) {
        setPath(NULL);
        return; 
    }
    pair<int,int> nextTile = path->getNext();
    if(x == nextTile.first && y == nextTile.second) {
        return;
    }
    if(isValid(nextTile.first,nextTile.second, gameMap)){
        gameMap.getTile(x,y)->setContainsEntity(false);
        x = nextTile.first;
        y = nextTile.second;
        gameMap.getTile(nextTile.first, nextTile.second)->setContainsEntity(true);
        gameMap.getTile(nextTile.first, nextTile.second)->setEntity(this);
        return;
    } 
    //path->reAdd(nextTile);
    //path->reAdd(pair<int,int>(x,y));
    //goTo(path->getTarget(),gameMap);
    setPath(NULL);
}

void Entity::goTo(Entity* e, Map gameMap) {
    vector<CardinalDir> directions;
    directions.push_back(NORTH); directions.push_back(SOUTH); directions.push_back(EAST); directions.push_back(WEST);
    if(absSingleDistance(this->x,e->getX()) > absSingleDistance(this->y,e->getY())) {
        if(e->getX() > this->x) {
            directions.erase(find(directions.begin(),directions.end(),EAST));
            directions.insert(directions.begin(),EAST);
        } else {
            directions.erase(find(directions.begin(),directions.end(),WEST));
            directions.insert(directions.begin(),WEST);
        }
        if(e->getY() > this->y) {
            directions.erase(find(directions.begin(),directions.end(),SOUTH));
            directions.insert(directions.begin()+1,SOUTH);
        } else {
            directions.erase(find(directions.begin(),directions.end(),NORTH));
            directions.insert(directions.begin()+1,NORTH);
        }
    } else {
        if(e->getY() > this->y) {
            directions.erase(find(directions.begin(),directions.end(),SOUTH));
            directions.insert(directions.begin(),SOUTH);
        } else {
            directions.erase(find(directions.begin(),directions.end(),NORTH));
            directions.insert(directions.begin(),NORTH);
        }
        if(e->getX() > this->x) {
            directions.erase(find(directions.begin(),directions.end(),EAST));
            directions.insert(directions.begin()+1,EAST);
        } else {
            directions.erase(find(directions.begin(),directions.end(),WEST));
            directions.insert(directions.begin()+1,WEST);
        }
    }
    for(int i = 0; i < (int)directions.size(); i++) {
        if(directions[i] == NORTH && isValid(x,y-1, gameMap)){
            gameMap.getTile(x,y)->setContainsEntity(false);
            y--;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == SOUTH && isValid(x,y+1, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            y++;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == EAST && isValid(x+1,y, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            x++;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == WEST && isValid(x-1,y, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            x--;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        }
    }   
}

void Entity::goRandomDirection(Map gameMap) {
    vector<CardinalDir> directions;
    directions.push_back(NORTH); directions.push_back(SOUTH); directions.push_back(EAST); directions.push_back(WEST);
    random_shuffle(directions.begin(), directions.end());
    for(int i = 0; i < (int)directions.size(); i++) {
        if(directions[i] == NORTH && isValid(x,y-1, gameMap)){
            gameMap.getTile(x,y)->setContainsEntity(false);
            y--;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == SOUTH && isValid(x,y+1, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            y++;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == EAST && isValid(x+1,y, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            x++;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == WEST && isValid(x-1,y, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            x--;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        }
    }   
}

void Entity::goAwayFrom(Entity* e, Map gameMap) {
    vector<CardinalDir> directions;
    directions.push_back(NORTH); directions.push_back(SOUTH); directions.push_back(EAST); directions.push_back(WEST);
    if(absSingleDistance(this->x,e->getX()) > absSingleDistance(this->y,e->getY())) {
        if(e->getX() > this->x) {
            directions.erase(find(directions.begin(),directions.end(),EAST));
            directions.insert(directions.begin(),EAST);
        } else {
            directions.erase(find(directions.begin(),directions.end(),WEST));
            directions.insert(directions.begin(),WEST);
        }
        if(e->getY() > this->y) {
            directions.erase(find(directions.begin(),directions.end(),SOUTH));
            directions.insert(directions.begin()+1,SOUTH);
        } else {
            directions.erase(find(directions.begin(),directions.end(),NORTH));
            directions.insert(directions.begin()+1,NORTH);
        }
    } else {
        if(e->getY() > this->y) {
            directions.erase(find(directions.begin(),directions.end(),SOUTH));
            directions.insert(directions.begin(),SOUTH);
        } else {
            directions.erase(find(directions.begin(),directions.end(),NORTH));
            directions.insert(directions.begin(),NORTH);
        }
        if(e->getX() > this->x) {
            directions.erase(find(directions.begin(),directions.end(),EAST));
            directions.insert(directions.begin()+1,EAST);
        } else {
            directions.erase(find(directions.begin(),directions.end(),WEST));
            directions.insert(directions.begin()+1,WEST);
        }
    }
    reverse(directions.begin(), directions.end());
    for(int i = 0; i < (int)directions.size(); i++) {
        if(directions[i] == NORTH && isValid(x,y-1, gameMap)){
            gameMap.getTile(x,y)->setContainsEntity(false);
            y--;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == SOUTH && isValid(x,y+1, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            y++;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == EAST && isValid(x+1,y, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            x++;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        } else if(directions[i] == WEST && isValid(x-1,y, gameMap)) {
            gameMap.getTile(x,y)->setContainsEntity(false);
            x--;
            gameMap.getTile(x,y)->setContainsEntity(true);
            gameMap.getTile(x,y)->setEntity(this);
            return;
        }
    }   
}