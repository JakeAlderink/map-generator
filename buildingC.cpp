#include "buildingC.h"

void BuildingC::finishedBuilding(Map gameMap, Team* handler) {
    Tile* tile = findValidAdjacent(getX(), getY(), gameMap);
    if(tile == NULL) { return; }
    if(rotateEntity == 0) {
        handler->add(new BuilderC(tile->getX(), tile->getY(), 100, 100, Builder, this->getColor()));
        rotateEntity = 1;
    } else if(rotateEntity == 1) {
        handler->add(new WarriorC(tile->getX(), tile->getY(), 100, 100, Warrior, this->getColor()));
        rotateEntity = 0;
    }
    tile->setContainsEntity(true);
    isBuilding = false;
}

void BuildingC::update(Map gameMap, Team* t, Teams handler) {
    if(!isBuilding) {
        if(t->getResources() >= 100){
            buildQueue = 0;
            buildQueueMax = 100;
            isBuilding = true;
            t->setResources(t->getResources()-100);
        }
    } else if(buildQueue >= buildQueueMax) {
        finishedBuilding(gameMap, t);
    } else {
        buildQueue += 1;
    }
    ColorWrapper enemyColor(RED);
    if(t->getColor() == enemyColor) {
        enemyColor.setColor(BLUE);
    }
    
    Entity* closestE = findClosestEnemy(enemyColor, this, handler, gameMap);
    float distanceToE = absDistance(this, closestE);
    if(distanceToE <= 15.0) {
        closestE->setHealth(closestE->getHealth() - damage);
        return;
    }
    return;
    
}