#ifndef PATH_H
#define PATH_H

#include "lib.h"
#include "entity.h"

class Entity;

class Path {
    private:
        Entity* target;
        vector<pair<int,int>> path;
    public:
        ~Path();
        Path(){target = NULL;}
        Path(Entity* t, vector<pair<int,int>> p) {target = t; path = p;}
        pair<int,int> getNext(){pair<int,int> last = path.at(path.size()-1); path.pop_back(); return last;}
        int getSize() { return path.size();}
        Entity* getTarget(){return target;}
        void reAdd(pair<int,int> coord){ path.push_back(coord); return;}
};


#endif