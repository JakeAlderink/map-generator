#include "saveState.h"

SaveState::SaveState(AStarAlgo algo, Entity* start, Tile* target, set<pair<float ,pair<int,int>>> sortedStack) {
    this->sortedStack = sortedStack;
    this->start = start;
    this->target = target;
    this->pathDistance = algo.getPathDistance();
    this->absoluteDistance = algo.getAbsoluteDistance();
    this->f = algo.getF();
    this->maxValue = algo.getMaxValue();
    this->width = algo.getWidth();
    this->height = algo.getHeight();
}