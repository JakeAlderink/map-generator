#ifndef COLORWRAPPER_H
#define COLORWRAPPER_H

#include "lib.h"

class ColorWrapper {
    private:
        Color color;
    public:
        ColorWrapper(){this->color = GRAY;}
        ColorWrapper(Color color){this->color = color;}
        Color getColor() { return color;}
        void setColor(Color c) { this->color = c;}
        bool operator==(const ColorWrapper& rhs) const{
            return (color.r == rhs.color.r && color.g == rhs.color.g && color.b == rhs.color.b && color.a == rhs.color.a);
        }
        bool operator!=(const ColorWrapper& rhs){ return !(*this == rhs); }
};

#endif