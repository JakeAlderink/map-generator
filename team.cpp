#include "team.h"

void Team::remove(int i) 
{ 
    Entity* e = entities->at(i); 
    entities->erase(entities->begin() + i); 
    delete e;
}


void Team::remove(Entity* e) 
{ 
    entities->erase(find(entities->begin(), entities->end(), e)); 
    delete e; 
}