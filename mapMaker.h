#ifndef MAPMAKER_H
#define MAPMAKER_H

#include "globals.h"
#include "lib.h"
#include "teams.h"
#include "colorWrapper.h"
#include "tile.h"
#include "entityType.h"
#include "map.h"
#include "path.h"
#include "entity.h"
#include "team.h"
#include "builderC.h"
#include "warriorC.h"
#include "resourceC.h"
#include "entry.h"
#include "cardinalDir.h"
#include "aStarAlgo.h"
#include "utils.h"
#include "drawUtils.h"
#include "mapGeneration.h"
int main();


#endif