#ifndef TEAMS_H
#define TEAMS_H

#include "lib.h"
#include "team.h"
#include "colorWrapper.h"


class Team;

class Teams {
    private:
        vector<Team*>* teams;
        vector<ColorWrapper>* colors;
    public:
        Teams();
        void add(Team* team);
        Team* getTeam(ColorWrapper color);
        Team* getTeam(int index) { return teams->at(index); }
        int getTeamSize(){ return teams->size(); }
        vector<Team*>* getTeams() { return teams; }
};
#endif