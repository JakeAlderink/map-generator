#ifndef UTILS_H
#define UTILS_H

#include "lib.h"
#include "map.h"
#include "tile.h"
#include "teams.h"

class Teams;
class Tile;
class Map;

bool isValid(int x, int y, Map gameMap);
double absDistance(Entity* e, Entity* k);
double absSingleDistance(float x, float y);
float absDistance(Tile* e, Entity* k);
float absDistance(Tile* e, Tile* k);
Tile* findValidAdjacent(int x, int y, Map gameMap);
Vector2 initVector2(float x, float y);
Tile* newBuildLocation(Map gameMap, Entity* BuildingBoi);
Entity* findClosestEntity(EntityType et, ColorWrapper c, Entity* currEntity, Teams handler, Map gameMap);
Entity* findClosestEnemy(ColorWrapper enemyColor, Entity* currEntity, Teams handler, Map gameMap);

#endif
